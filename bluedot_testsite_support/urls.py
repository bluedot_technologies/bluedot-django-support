from django.conf.urls import include, url
from django.http import HttpResponse


urlpatterns = [
    url(r'^$', lambda request: HttpResponse('BlueDot Support testsite main page.')),
    url(r'^przemocnie/', include('bluedot_django_przemocnie.urls')),
    url(r'^_publications/', include('bluedot_django_publication.urls')),
    url(r'^_auth/', include('bluedot_django_support_auth.urls')),
]

