from django.apps import AppConfig


class BluedotDjangoPublicationConfig(AppConfig):
    name = 'bluedot_django_publication'
