import json

from django.http import HttpResponse, HttpResponseForbidden
from django.shortcuts import render
from django.urls import reverse


def home_view(request):
    '''Shows home page'''

    if not request.user.is_staff:
        return HttpResponseForbidden('You are not a staff member!')

    # FIXME: Is there better way for generating partial edit address?
    context = { 'contents': { 'publications': {
            'api_address': reverse('publications_api', kwargs={}),
            'edit_address': reverse('publications_home', kwargs={}) + 'edit/',
        } } }
    return render(request, 'bluedot_django_publication/home.html', context)


def edit_view(request, **ids):
    '''Shows edit page'''

    if not request.user.is_staff:
        return HttpResponseForbidden('You are not a staff member!')

    context = { 'contents': { 'publications': {
            'api_address': reverse('publications_api', kwargs={'publications_id': ids['edit_id']}),
        } } }
    return render(request, 'bluedot_django_publication/edit.html', context)

