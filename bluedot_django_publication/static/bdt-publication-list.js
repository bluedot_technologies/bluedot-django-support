'use strict';

class LoadingState extends bd.State {
    constructor(view) {
        super(view, 'LoadingState');
        this.success_code = 200;
    }

    enter() {
        this.view.set_status_text('Loading publications…')
        let callbacks = this.make_callbacks();
        bd.q('GET', this.view.api_address, callbacks);
    }

    fetched(response) {
        this.view.set_state(new MainState(this.view, response.publications))
    }

    forward() {}
}

class MainState extends bd.State {
    constructor(view, publications) {
        super(view, 'MainState');
        this.success_code = 201;
        this.publications = publications;
    }

    enter() {
        this.view.set_creation_button_text('Create new publication')
        this.view.refresh_publications(this.publications)
    }

    forward() {
        this.view.set_state(new CreationState(this.view, this.publications))
    }
}

class CreationState extends bd.State {
    constructor(view) {
        super(view, 'CreationState');
        this.success_code = 200;
    }

    enter() {
        this.view.set_creation_button_text('Creating new publication…')
        let data = {
                title: '<title>',
                summary: '<summary>',
                text: '<text>',
                published: false,
            };
        let callbacks = this.make_callbacks();
        bd.q('PUT', this.view.api_address, callbacks, data);
    }

    fetched(response) {
        this.view.set_state(new LoadingState(this.view))
    }

    forward() {}
}

class DeletingState extends bd.State {
    constructor(view, publication_id) {
        super(view, 'CreationState');
        this.success_code = 200;
        this.publication_id = publication_id;
    }

    enter() {
        this.view.set_status_text('Deleting publication…')
        let callbacks = this.make_callbacks();
        bd.q('DELETE', this.view.get_api_publication_address(this.publication_id), callbacks);
    }

    fetched(response) {
        this.view.set_state(new LoadingState(this.view))
    }

    forward() {}
}

class PublicationViewer {
    constructor(data) {
        this.placeholder_id = 'publications-placeholder';
        this.creation_button_id = 'new-publication-button';
        this.data = data;
    }

    initialize() {
        this.bind_events();
        this.set_state(new LoadingState(this));
    }

    set_state(state) {
        console.log('Enter state "' + state.name + '"');
        this.state = state;
        this.state.enter();
    }

    set_status_text(text) {
        bd.e(this.placeholder_id).set_html(text);
    }

    set_creation_button_text(text) {
        bd.e(this.creation_button_id).set_html(text);
    }

    get api_address() {
        return this.data.api_address;
    }

    get_api_publication_address(publication_id) {
        return this.data.api_address + publication_id;
    }

    get_edit_address(publication_id) {
        return this.data.edit_address + publication_id;
    }

    delete(publication_id) {
        if (confirm('Are you sure you want to delete this publication?')) {
            this.set_state(new DeletingState(this, publication_id));
        }
    }

    edit(publication_id) {
        location = this.get_edit_address(publication_id);
    }

    refresh_publications(publications) {
        let html = '';
        if (publications.length == 0) {
            html = 'You have no publications yet.';
        } else {
            html  = '<ul class="bdt-elements">';
            for (let i in publications) {
                let publication = publications[i];
                html += '<li class="bdt-publication bdt-publication-';
                html += publication.published ? 'active' : 'inactive';
                html += '"><span class="bdt-publication-buttons">';
                html += '<a href="#" onclick="publication_viewer.delete(\''
                html += publication.id;
                html += '\')">DELETE</a><a href="#" onclick="publication_viewer.edit(\''
                html += publication.id;
                html += '\')">EDIT</a>';
                html += '</span><div class="bdt-publication-title">';
                html += bd.h(publication.title);
                html += '</div><div class="bdt-publication-summary">';
                html += publication.summary_html;
                html += '</div></li>';
            }
            html += '</ul>';
        }

        bd.e(this.placeholder_id).set_html(html);
    }

    bind_events() {
        let view = this;

        let forward_button = bd.e(this.creation_button_id);
        forward_button.bind('click', function() {
            view.state.forward();
        });
    }
}

let publication_viewer = new PublicationViewer(document.PUBLICATIONS_INFO);
publication_viewer.initialize();

