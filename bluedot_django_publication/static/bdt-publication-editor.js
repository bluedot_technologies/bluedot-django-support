'use strict';

class PublicationData {
    constructor(title, summary, text, published) {
        this.title = title;
        this.summary = summary;
        this.text = text;
        this.published = published;
    }

    is_valid() {
        return (this.title != '') && (this.summary != '') && (this.text != '');
    }

    is_same(other) {
        return (this.title == other.title)
            && (this.summary == other.summary)
            && (this.text == other.text)
            && (this.published == other.published);
    }

    publication_state_changed(other) {
        return this.published != other.published;
    }

    as_json() {
        return {
            title: this.title,
            summary: this.summary,
            text: this.text,
            published: this.published,
        }
    }
}

class LoadingState extends bd.State {
    constructor(view) {
        super(view, 'LoadingState');
        this.success_code = 200;
    }

    enter() {
        let callbacks = this.make_callbacks();
        bd.q('GET', this.view.api_address, callbacks);
    }

    fetched(resp) {
        let pub = new PublicationData(resp.title, resp.summary_rst, resp.text_rst, resp.published);
        this.view.refresh(pub);
        this.view.set_state(new MainState(this.view))
    }
}

class MainState extends bd.State {
    constructor(view, publications) {
        super(view, 'MainState');
        this.publications = publications;
    }

    enter() {
        this.view.set_save_button_text('Save')
    }
}

class SavingState extends bd.State {
    constructor(view, publication) {
        super(view, 'SavingState');
        this.success_code = 200;
        this.publication = publication;
    }

    enter() {
        this.view.set_save_button_text('Saving the publication…')
        let data = this.publication.as_json();
        let callbacks = this.make_callbacks();
        bd.q('PATCH', this.view.api_address, callbacks, data);
    }

    fetched(response) {
        this.view.last_publication = this.publication;
        this.view.set_state(new MainState(this.view));
    }

    forward() {}
}

class PublicationEditor {
    constructor(data) {
        this.save_button_id = 'save-button';
        this.title_field_id = 'publication-title';
        this.summary_field_id = 'publication-summary';
        this.text_field_id = 'publication-text';
        this.publication_checkbox_id = 'publish-checkbox';
        this.data = data;
    }

    initialize() {
        this.set_state(new LoadingState(this));
    }

    set_state(state) {
        console.log('Enter state "' + state.name + '"');
        this.state = state;
        this.state.enter();
    }

    make_publication() {
        let title = bd.e(this.title_field_id).get_value();
        let summary = bd.e(this.summary_field_id).get_value();
        let text = bd.e(this.text_field_id).get_value();
        let published = bd.e(this.publication_checkbox_id).is_checked();
        return new PublicationData(title, summary, text, published);
    }

    save(publication_id) {
        let new_publication = this.make_publication();
        if (new_publication.is_valid()) {
            if (!new_publication.is_same(this.last_publication)) {
                let changed = new_publication.publication_state_changed(this.last_publication);
                if (!changed || confirm('Are you sure you want to (un)publish?')) {
                    this.set_state(new SavingState(this, new_publication));
                }
            } else {
                alert('Contents did not change.');
            }
        } else {
            alert('Publication data is invalid. Fields cannot be empty.');
        }
    }

    get api_address() {
        return this.data.api_address;
    }

    set_status_text(text) {}

    set_save_button_text(text) {
        bd.e(this.save_button_id).set_html(text);
    }

    refresh(publication) {
        bd.e(this.title_field_id).set_value(publication.title);
        bd.e(this.summary_field_id).set_value(publication.summary);
        bd.e(this.text_field_id).set_value(publication.text);
        bd.e(this.publication_checkbox_id).set_checked(publication.published);

        this.last_publication = publication;
    }
}

let publication_editor = new PublicationEditor(document.PUBLICATIONS_INFO);
publication_editor.initialize();

