import uuid

from django.contrib.auth import models as auth_models
from django.db import models


class Publication(models.Model):
    '''Model for storing publications'''

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    title = models.TextField()
    summary = models.TextField()
    text = models.TextField()
    published = models.BooleanField()

    publisher = models.ForeignKey(auth_models.User, on_delete=models.CASCADE, related_name='+')
    publication_datetime = models.DateTimeField(auto_now_add=True)
    last_editor = models.ForeignKey(auth_models.User, on_delete=models.CASCADE, related_name='+')
    last_edition_datetime = models.DateTimeField(auto_now_add=True)

