from datetime import datetime

from django.core.exceptions import ObjectDoesNotExist

from bluedot_django_common.utils import rst2html, uuid2str
from bluedot_django_restapi import provider
from bluedot_django_restapi.decorators import decode_as_json, exact_fields, supported_fields
from bluedot_django_restapi.response import Response as R

from . import models


class PublicationCollectionProvider(provider.ApiProvider):
    '''REST API provider for `publications` model.'''

    QUERY_FIELDS = {'title', 'summary', 'text', 'published'}
    METHOD_DECLARATIONS = ('POST', 'GET')
    _MODEL = models.Publication

    def __init__(self, entries):
        self._entries = entries

    @classmethod
    def load(cls, request):
        try:
            entries = cls._MODEL.objects.all()
        except ObjectDoesNotExist:
            return None
        else:
            return cls(entries)

    def _create(self, request, query):
        '''Creates new publication entry.'''

        publication = self._MODEL()
        publication.title = query['title']
        publication.summary = query['summary']
        publication.text = query['text']
        publication.published = query['published']
        publication.publisher = request.user
        publication.publication_datetime = datetime.now()
        publication.last_editor = request.user
        publication.last_edition_datetime = datetime.now()
        publication.save()

        return R.Ok({ 'new_id': uuid2str(publication.id) })

    def _get(self):
        '''Returns list of current publications.'''

        publications = []
        for entry in sorted(self._entries, key=lambda e: e.publication_datetime, reverse=True):
            publications.append({
                    'id': uuid2str(entry.id),
                    'title': entry.title,
                    'summary_rst': entry.summary,
                    'summary_html': rst2html(entry.summary),
                    'text_rst': entry.text,
                    'text_html': rst2html(entry.text),
                    'published': entry.published,
                    'publisher': entry.publisher.get_full_name(),
                    'publication_datetime': entry.publication_datetime.isoformat(),
                    'last_editor': entry.last_editor.get_full_name(),
                    'last_edition_datetime': entry.last_edition_datetime.isoformat(),
                })

        return R.Ok({ 'publications': publications })

    @decode_as_json
    @exact_fields(QUERY_FIELDS)
    def post(self, request, query):
        return self._create(request, query)

    def get(self, request):
        return self._get()

    def get_create_access(self, request):
        return request.user.is_staff

    def get_read_access(self, request):
        return request.user.is_staff


class PublicationObjectProvider(provider.ApiProvider):
    '''REST API provider for `publications` model.'''

    QUERY_FIELDS = {'title', 'summary', 'text', 'published'}
    METHOD_DECLARATIONS = ('GET', 'PATCH', 'DELETE')
    _MODEL = models.Publication

    def __init__(self, entry):
        self._entry = entry

    @classmethod
    def load(cls, request, **ids):
        try:
            entry = cls._MODEL.objects.get(id=ids['publications_id'])
        except ObjectDoesNotExist:
            return None
        else:
            return cls(entry)

    def _get(self):
        '''Returns information about specified publication.'''

        return R.Ok({
                'id': uuid2str(self._entry.id),
                'title': self._entry.title,
                'summary_rst': self._entry.summary,
                'summary_html': rst2html(self._entry.summary),
                'text_rst': self._entry.text,
                'text_html': rst2html(self._entry.text),
                'published': self._entry.published,
                'publisher': self._entry.publisher.get_full_name(),
                'publication_datetime': self._entry.publication_datetime.isoformat(),
                'last_editor': self._entry.last_editor.get_full_name(),
                'last_edition_datetime': self._entry.last_edition_datetime.isoformat(),
            })

    def _update(self, request, query):
        '''Updates publication basing on given query.'''

        changed = False

        if 'title' in query and self._entry.title != query['title']:
            self._entry.title = query['title']
            changed = True

        if 'summary' in query and self._entry.summary != query['summary']:
            self._entry.summary = query['summary']
            changed = True

        if 'text' in query and self._entry.text != query['text']:
            self._entry.text = query['text']
            changed = True

        if 'published' in query and self._entry.published != query['published']:
            self._entry.published = query['published']
            if query['published']:
                self._entry.publisher = request.user
                self._entry.publication_datetime = datetime.now()
            changed = True

        if changed:
            self._entry.last_editor = request.user
            self._entry.last_edition_datetime = datetime.now()
            self._entry.save()

        return R.Ok({})

    def _delete(self):
        '''Deletes specified publication.'''

        self._entry.delete()
        return R.Ok({})

    def get(self, request, **ids):
        return self._get()

    @decode_as_json
    @supported_fields(QUERY_FIELDS)
    def patch(self, request, query):
        return self._update(request, query)

    def delete(self, request):
        return self._delete()

    def get_read_access(self, request, **ids):
        return request.user.is_staff

    def get_write_access(self, request, **ids):
        return request.user.is_staff


PublicationCollectionProvider.define_methods()
PublicationObjectProvider.define_methods()

