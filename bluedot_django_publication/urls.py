from django.conf.urls import url

from bluedot_django_restapi.dispatcher import make_url_patterns

from . publication_providers import PublicationCollectionProvider, PublicationObjectProvider
from . import views

urlpatterns = [
    url(
        r'^$',
        views.home_view,
        name='publications_home'
    ),
    url(
        r'^edit/(?P<edit_id>[0-9a-f]+)/$',
        views.edit_view,
        name='publication_edit'
    ),
]

urlpatterns.extend(
    make_url_patterns(
        r'^api/v1',
        {
            'publications': (PublicationCollectionProvider, PublicationObjectProvider),
            #'converter': (ConverterProvider, None),
        }
    )
)

