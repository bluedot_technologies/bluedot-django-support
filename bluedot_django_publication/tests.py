import json

from django.contrib.auth.models import User
from django.test import TestCase


class PublicationTests(TestCase):
    '''Tests for `bludedot_django_publication` application.'''

    fixtures = ['basic.yaml']

    ARTICLE1_TITLE = 'title1'
    ARTICLE1_SUMMARY = 'summary1'
    ARTICLE1_TEXT = 'text1'
    ARTICLE2_TITLE = 'title2'
    ARTICLE2_SUMMARY = 'summary2'
    ARTICLE2_TEXT = 'text2'
    ARTICLE3_TITLE = 'title3'
    ARTICLE3_SUMMARY = 'summary3'
    ARTICLE3_TEXT = 'text3'

    def test_index_page_loads(self):
        '''Check if index page loads correctly with roper permissions.'''

        response = self.client.get('/_publications/')
        self.assertEqual(response.status_code, 403, response.content)

        self.client.force_login(User.objects.get_or_create(username='Eve')[0])

        response = self.client.get('/_publications/')
        self.assertEqual(response.status_code, 403, response.content)

        self.client.force_login(User.objects.get_or_create(username='Alice')[0])

        response = self.client.get('/_publications/')
        self.assertEqual(response.status_code, 200, response.content)

    def test_edit_page_loads(self):
        '''Check if edit page loads correctly with roper permissions.'''

        response = self.client.get('/_publications/edit/10000000000000000000000000000000/')
        self.assertEqual(response.status_code, 403, response.content)

        self.client.force_login(User.objects.get_or_create(username='Eve')[0])

        response = self.client.get('/_publications/edit/10000000000000000000000000000000/')
        self.assertEqual(response.status_code, 403, response.content)

        self.client.force_login(User.objects.get_or_create(username='Alice')[0])

        response = self.client.get('/_publications/edit/10000000000000000000000000000000/')
        self.assertEqual(response.status_code, 200, response.content)

    def test_publication_collection_api_loads(self):
        '''Check if publications collection API loads correctly with roper permissions.'''

        response = self.client.get('/_publications/api/v1/publications/')
        self.assertEqual(response.status_code, 403, response.content)

        self.client.force_login(User.objects.get_or_create(username='Eve')[0])

        response = self.client.get('/_publications/api/v1/publications/')
        self.assertEqual(response.status_code, 403, response.content)

        self.client.force_login(User.objects.get_or_create(username='Alice')[0])

        response = self.client.get('/_publications/api/v1/publications/')
        self.assertEqual(response.status_code, 200, response.content)

    def test_publication_object_api_loads(self):
        '''Check if publication object API loads correctly with roper permissions.'''

        response = self.client.get('/_publications/api/v1/publications/10000000000000000000000000000000/')
        self.assertEqual(response.status_code, 403, response.content)

        self.client.force_login(User.objects.get_or_create(username='Eve')[0])

        response = self.client.get('/_publications/api/v1/publications/10000000000000000000000000000000/')
        self.assertEqual(response.status_code, 403, response.content)

        self.client.force_login(User.objects.get_or_create(username='Alice')[0])

        response = self.client.get('/_publications/api/v1/publications/10000000000000000000000000000000/')
        self.assertEqual(response.status_code, 200, response.content)

    def test_getting_publications_collection(self):
        '''Check if publications collection API GET call returns correct contents.'''

        self.client.force_login(User.objects.get_or_create(username='Alice')[0])
        response = self.client.get('/_publications/api/v1/publications/')
        self.assertEqual(response.status_code, 200, response.content)

        data = json.loads(response.content)
        self.assertTrue(len(data['publications']) == 3, data)

        pub1 = data['publications'][2]
        pub2 = data['publications'][1]
        pub3 = data['publications'][0]

        self.assertEqual(pub1['title'], self.ARTICLE1_TITLE, data)
        self.assertEqual(pub1['summary_rst'], self.ARTICLE1_SUMMARY, data)
        self.assertEqual(pub1['text_rst'], self.ARTICLE1_TEXT, data)
        self.assertEqual(pub2['title'], self.ARTICLE2_TITLE, data)
        self.assertEqual(pub2['summary_rst'], self.ARTICLE2_SUMMARY, data)
        self.assertEqual(pub2['text_rst'], self.ARTICLE2_TEXT, data)
        self.assertEqual(pub3['title'], self.ARTICLE3_TITLE, data)
        self.assertEqual(pub3['summary_rst'], self.ARTICLE3_SUMMARY, data)
        self.assertEqual(pub3['text_rst'], self.ARTICLE3_TEXT, data)

    def test_getting_publications_object(self):
        '''Check if publication object API GET call returns correct contents.'''

        self.client.force_login(User.objects.get_or_create(username='Alice')[0])
        response = self.client.get('/_publications/api/v1/publications/10000000000000000000000000000000/')
        self.assertEqual(response.status_code, 200, response.content)

        data = json.loads(response.content)
        self.assertEqual(data['title'], self.ARTICLE1_TITLE, data)
        self.assertEqual(data['summary_rst'], self.ARTICLE1_SUMMARY, data)
        self.assertEqual(data['text_rst'], self.ARTICLE1_TEXT, data)

