from django.apps import AppConfig


class BluedotDjangoSupportAuthConfig(AppConfig):
    name = 'bluedot_django_support_auth'
