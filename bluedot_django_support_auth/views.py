from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render
from django.urls import reverse
from django.views.generic.edit import CreateView


def home_view(request):
    '''Shows home page'''

    context = {}
    return render(request, 'bluedot_django_support_auth/home.html', context)


def register_view(request):
    return CreateView.as_view(
            template_name='bluedot_django_support_auth/register.html',
            form_class=UserCreationForm,
            success_url=reverse('bluedot_django_support_auth_home'),
        ).__call__(request)

