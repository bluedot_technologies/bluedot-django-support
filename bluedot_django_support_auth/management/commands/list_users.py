from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User

class Command(BaseCommand):
    help = 'List all publishers'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        users = User.objects.all()

        if len(users):
            for user in users:
                self.stdout.write(f'Login: "{user.username}"')
                self.stdout.write(f'First name: "{user.first_name}"')
                self.stdout.write(f'Last name: "{user.last_name}"')
                self.stdout.write(f'Is active: {user.is_active}')
                self.stdout.write(f'Is superuser: {user.is_superuser}')
                self.stdout.write(f'Is staff: {user.is_staff}')
                self.stdout.write('')
        else:
            self.stdout.write('No users were found.')
