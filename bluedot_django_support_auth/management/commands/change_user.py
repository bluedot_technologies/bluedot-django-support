from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import BaseCommand, CommandError

class Command(BaseCommand):
    help = 'Changes permissions for publisher'

    def add_arguments(self, parser):
        parser.add_argument('username', nargs='+', type=str)
        parser.add_argument(
            '--superuser',
            dest='superuser',
            choices=['true', 'false'],
            type=str,
            help='Makes the user a superuser')
        parser.add_argument(
            '--staff',
            dest='staff',
            choices=['true', 'false'],
            type=str,
            help='Add the user to staff')
        parser.add_argument(
            '--first_name',
            dest='first_name',
            type=str,
            help='Set first name')
        parser.add_argument(
            '--last_name',
            dest='last_name',
            type=str,
            help='Set last name')

    def handle(self, *args, **options):
        username = options['username'][0]
        try:
            user = User.objects.get(username=username)
        except ObjectDoesNotExist:
            raise CommandError(f'User "{username}" does not exist')

        action = False

        superuser = options['superuser']
        staff = options['staff']
        first_name = options['first_name']
        last_name = options['last_name']

        if superuser is not None:
            self.stdout.write(f'Changing superuser to "{superuser}"')
            user.is_superuser = superuser == 'true'
            action = True

        if staff is not None:
            self.stdout.write(f'Changing staff to "{staff}"')
            user.is_staff = staff == 'true'
            action = True

        if first_name is not None:
            self.stdout.write(f'Changing first name to "{first_name}"')
            user.first_name = first_name
            action = True

        if last_name is not None:
            self.stdout.write(f'Changing last name to "{last_name}"')
            user.last_name = last_name
            action = True

        if action:
            user.save()
        else:
            self.stdout.write('No actions performed')

