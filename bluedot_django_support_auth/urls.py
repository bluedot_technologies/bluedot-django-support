from django.conf.urls import url
from django.contrib.auth import views as auth_views

from . import views


urlpatterns = [
    url(r'^$', views.home_view, name='bluedot_django_support_auth_home'),
    url(r'^register/$', views.register_view, name='bluedot_django_support_auth_register'),
    url(
        r'^login/$',
        auth_views.LoginView.as_view(template_name='bluedot_django_support_auth/login.html'),
        name='bluedot_django_support_auth_login'
    ),
    url(
        r'^logout/$',
        auth_views.LogoutView.as_view(template_name='bluedot_django_support_auth/logout.html'),
        name='bluedot_django_support_auth_logout'
    )
]

