import os
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

packages = [
    'bluedot_django_publication',
    'bluedot_django_publication.migrations',
    'bluedot_django_support_auth',
    'bluedot_django_support_auth.management',
    'bluedot_django_support_auth.management.commands',
    'bluedot_django_przemocnie'
]

setup(
    name='bluedot_django_support',
    version='0.1.0',
    packages=packages,
    include_package_data=True,
    license='MPL 2.0',
    description='A set of `django` applications used for BlueDot support projects',
    long_description=README,
    url='https://bluedot.community/',
    author='Wojciech Kluczka',
    author_email='wojciech.kluczka@gmail.com',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Framework :: Django :: 1.11',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Mozilla Public License 2.0 (MPL 2.0)',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content :: Content Management System'
    ],
)

