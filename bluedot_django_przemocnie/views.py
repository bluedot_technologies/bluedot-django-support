from docutils.core import publish_parts

from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.shortcuts import render

from bluedot_django_common.utils import rst2html, uuid2str
from bluedot_django_publication.models import Publication


def home_view(request):
    '''Shows home visit page'''

    context = {}
    return render(request, 'bluedot_django_przemocnie/home.html', context)


def news_view(request):
    '''Shows news page'''

    entries = Publication.objects.filter(published__exact=True) \
                                 .select_related('publisher', 'last_editor') \
                                 .order_by('-publication_datetime')

    news = [{
            'id': uuid2str(entry.id),
            'title': entry.title,
            'summary': rst2html(entry.summary),
            'publisher': entry.publisher.get_full_name(),
            'publication_datetime': entry.publication_datetime,
            'last_editor': entry.last_editor.get_full_name(),
            'last_edition_datetime': entry.last_edition_datetime,
        } for entry in entries]

    context = { 'contents': { 'news': news } }
    return render(request, 'bluedot_django_przemocnie/news.html', context)


def article_view(request, resource_id):
    '''Shows article page'''

    EMPTY_CONTEXT = { 'contents': { 'news_found': False } }

    try:
        entry = Publication.objects.select_related('publisher', 'last_editor').get(id=resource_id)

    except ObjectDoesNotExist:
        context = EMPTY_CONTEXT

    except ValidationError:
        context = EMPTY_CONTEXT

    else:
        if entry.published:
            context = { 'contents': {
                'news_found': True,
                'news': {
                    'title': entry.title,
                    'summary': rst2html(entry.summary),
                    'text': rst2html(entry.text),
                    'publisher': entry.publisher.get_full_name(),
                    'publication_datetime': entry.publication_datetime,
                    'last_editor': entry.last_editor.get_full_name(),
                    'last_edition_datetime': entry.last_edition_datetime,
                } } }
        else:
            context = EMPTY_CONTEXT

    return render(request, 'bluedot_django_przemocnie/article.html', context)

