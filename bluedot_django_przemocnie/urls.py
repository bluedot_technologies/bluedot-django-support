from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^$', views.home_view, name='przemocnie_home'),
    url(r'^news/$', views.news_view, name='przemocnie_news'),
    url(r'^article/(?P<resource_id>[0-9a-f]+)/$', views.article_view, name='przemocnie_article'),
]

