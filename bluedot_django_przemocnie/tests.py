from django.test import TestCase


class PrzemocnieTests(TestCase):
    '''Tests `bluedot_django_przemocnie` application.'''

    fixtures = ['basic.yaml']

    CORRECT_CONTENT_MATCH = 'property="og:url" content="http://przemocnie.org/">'
    NOT_EXISTING_ARTICLE_MATCH = 'The article you are looking for does not exist'
    ARTICLE1_TITLE = 'title1'
    ARTICLE1_SUMMARY = 'summary1'
    ARTICLE1_TEXT = 'text1'
    ARTICLE2_TITLE = 'title2'
    ARTICLE2_SUMMARY = 'summary2'
    ARTICLE2_TEXT = 'text2'
    ARTICLE3_TITLE = 'title3'
    ARTICLE3_SUMMARY = 'summary3'
    ARTICLE3_TEXT = 'text3'

    def test_main_page_loads(self):
        '''Check if main page loads correctly'''

        response = self.client.get('/przemocnie/')
        self.assertEqual(response.status_code, 200, response.content)
        self.assertContains(response, self.CORRECT_CONTENT_MATCH)

    def test_news_page_loads(self):
        '''Check if news page loads correctly'''

        response = self.client.get('/przemocnie/news/')
        self.assertEqual(response.status_code, 200, response.content)
        self.assertContains(response, self.CORRECT_CONTENT_MATCH)

        self.assertContains(response, self.ARTICLE1_TITLE)
        self.assertContains(response, self.ARTICLE1_SUMMARY)
        self.assertNotContains(response, self.ARTICLE1_TEXT)
        self.assertContains(response, self.ARTICLE2_TITLE)
        self.assertContains(response, self.ARTICLE2_SUMMARY)
        self.assertNotContains(response, self.ARTICLE2_TEXT)
        self.assertNotContains(response, self.ARTICLE3_TITLE)
        self.assertNotContains(response, self.ARTICLE3_SUMMARY)
        self.assertNotContains(response, self.ARTICLE3_TEXT)

    def test_article_page_loads(self):
        '''Check if article page loads correctly'''

        response = self.client.get('/przemocnie/article/10000000000000000000000000000000/')
        self.assertEqual(response.status_code, 200, response.content)
        self.assertContains(response, self.CORRECT_CONTENT_MATCH)

        self.assertContains(response, self.ARTICLE1_TITLE)
        self.assertContains(response, self.ARTICLE1_SUMMARY)
        self.assertContains(response, self.ARTICLE1_TEXT)
        self.assertNotContains(response, self.ARTICLE2_TITLE)
        self.assertNotContains(response, self.ARTICLE2_SUMMARY)
        self.assertNotContains(response, self.ARTICLE2_TEXT)
        self.assertNotContains(response, self.ARTICLE3_TITLE)
        self.assertNotContains(response, self.ARTICLE3_SUMMARY)
        self.assertNotContains(response, self.ARTICLE3_TEXT)

    def test_not_existing_article(self):
        '''Check if news page for not existing article loads correctly'''

        response = self.client.get('/przemocnie/article/99999999999999999999999999999999/')
        self.assertEqual(response.status_code, 200, response.content)
        self.assertContains(response, self.CORRECT_CONTENT_MATCH)
        self.assertContains(response, self.NOT_EXISTING_ARTICLE_MATCH)

        self.assertNotContains(response, self.ARTICLE1_TITLE)
        self.assertNotContains(response, self.ARTICLE1_SUMMARY)
        self.assertNotContains(response, self.ARTICLE1_TEXT)
        self.assertNotContains(response, self.ARTICLE2_TITLE)
        self.assertNotContains(response, self.ARTICLE2_SUMMARY)
        self.assertNotContains(response, self.ARTICLE2_TEXT)
        self.assertNotContains(response, self.ARTICLE3_TITLE)
        self.assertNotContains(response, self.ARTICLE3_SUMMARY)
        self.assertNotContains(response, self.ARTICLE3_TEXT)

    def test_not_published_article(self):
        '''Check if article page for unpublished article loads correctly'''

        response = self.client.get('/przemocnie/article/30000000000000000000000000000000/')
        self.assertEqual(response.status_code, 200, response.content)
        self.assertContains(response, self.CORRECT_CONTENT_MATCH)
        self.assertContains(response, self.NOT_EXISTING_ARTICLE_MATCH)

        self.assertNotContains(response, self.ARTICLE1_TITLE)
        self.assertNotContains(response, self.ARTICLE1_SUMMARY)
        self.assertNotContains(response, self.ARTICLE1_TEXT)
        self.assertNotContains(response, self.ARTICLE2_TITLE)
        self.assertNotContains(response, self.ARTICLE2_SUMMARY)
        self.assertNotContains(response, self.ARTICLE2_TEXT)
        self.assertNotContains(response, self.ARTICLE3_TITLE)
        self.assertNotContains(response, self.ARTICLE3_SUMMARY)
        self.assertNotContains(response, self.ARTICLE3_TEXT)

